import styled from "styled-components";

export const StyledContent = styled.main`
  width: 90vw;
  margin: 0 auto;

  margin-top: 1em;
  padding: 1em;

  background-color: var(--surface-f);
  border-radius: 4px;
`;
