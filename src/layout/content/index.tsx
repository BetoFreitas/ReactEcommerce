import { FC } from "react";
import { ContentProps } from "./types";
import { StyledContent } from "./styles";

export const Content: FC<ContentProps> = (props) => {
  const { children } = props;
  return <StyledContent>{children}</StyledContent>;
};
