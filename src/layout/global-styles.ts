import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-family: var(--font-family);
    }

    body {
        min-width: 100vw;
        width: 100vw;
        max-width: 100vw;
        overflow-x: hidden;

        min-height: 100vh;
        height: 100vh;
    }
`;
