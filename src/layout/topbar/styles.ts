import styled from "styled-components";

export const StyledTopbar = styled.nav`
  position: sticky;
  top: 0;

  width: 100vw;
  background-color: var(--primary-color);
  color: var(--text-color);

  min-height: 3rem;

  display: flex;
  align-items: center;
  padding: 10px;
`;
