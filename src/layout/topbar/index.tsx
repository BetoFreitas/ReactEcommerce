import { Logo } from "./common/logo";
import { StyledTopbar } from "./styles";

export const Tobpar = () => {
  return (
    <StyledTopbar>
      <Logo />
    </StyledTopbar>
  );
};
