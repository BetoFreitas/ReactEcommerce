import axios from "axios";
import { FC } from "react";
import { Product } from "../../models/product";
import { useQuery } from "react-query";
import { Card } from "primereact/card";
import { Skeleton } from "primereact/skeleton";

const CardLoadingTemplate = () => {
  return (
    <Card
      title={<Skeleton width="80%" height="25px" />}
      subTitle={<Skeleton width="80%" height="17px" />}
      className="col-6 md:col-3"
      header={<Skeleton height="100px" />}
    >
      <p className="m-0 flex gap-1 flex-column" style={{ lineHeight: "1.5" }}>
        <Skeleton height="17px" width="90%" />
        <Skeleton height="17px" width="70%" />
        <Skeleton height="17px" width="90%" />
        <Skeleton height="17px" width="70%" />
      </p>
    </Card>
  );
};

const wait = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));

const fetchProducts = async () => {
  await wait(1000);
  return (
    await axios.get<{ products: Product[] }>(
      "https://dummyjson.com/products?limit=10"
    )
  ).data.products;
};

export const ShopPage: FC = () => {
  const { data: products, isFetching } = useQuery("products", fetchProducts, {
    refetchOnWindowFocus: false,
  });

  return (
    <div className="grid grid-no-gutter justify-content-center">
      {isFetching ? (
        <>
          {[0, 1, 2, 3].map((number) => (
            <CardLoadingTemplate key={number} />
          ))}
        </>
      ) : (
        products?.map((product) => (
          <Card
            title={product.title}
            subTitle={product.category}
            key={product.id}
            className="col-6 md:col-3"
            header={
              <img
                src={product.thumbnail}
                className="max-w-full border-round-lg"
                style={{
                  height: "100px",
                }}
                alt="Product image"
              />
            }
          >
            <p className="m-0" style={{ lineHeight: "1.5" }}>
              {product.description}
            </p>
          </Card>
        ))
      )}
    </div>
  );
};
