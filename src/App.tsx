import PrimeReact from "primereact/api";
import "primereact/resources/themes/mdc-dark-deeppurple/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import "/node_modules/primeflex/primeflex.css";
import { Tobpar } from "./layout/topbar";
import { Content } from "./layout/content";
import { ShopPage } from "./pages/shop";

export const App = () => {
  PrimeReact.ripple = true;

  return (
    <>
      <Tobpar />
      <Content>
        <ShopPage />
      </Content>
    </>
  );
};
