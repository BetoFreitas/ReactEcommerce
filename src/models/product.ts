import { Categories } from "./categories";

export type Product = {
  id: number; // number
  title: string; // "iPhone 9"
  description: string; // "An apple mobile which is nothing like apple"
  price: number; // 549
  discountPercentage: number; // 12.96
  rating: number; // 4.3
  stock: number; // 96
  brand: string; // "Apple"
  category: Categories; // "smartphones"
  thumbnail: string; // "http://url/to/image.jpg"
  images: string[]; // ["http://url/to/image.jpg"]
};
